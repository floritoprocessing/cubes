package graf.cubes;

import java.util.ArrayList;

public class Cube {

	/**
	 * OK result of the {@link #placeInSpace(boolean[])} Method
	 */
	private static final int OK = 0;
	
	/**
	 * OCCUPIED result of the {@link #placeInSpace(boolean[])} Method
	 */
	private static final int OCCUPIED = 1;
	
	/**
	 * OUT_OF_BOUNDS result of the {@link #placeInSpace(boolean[])} Method
	 */
	private static final int OUT_OF_BOUNDS = 2;
	
	/**
	 * Cube orientations
	 */
	public static final int X_AXIS=0, Y_AXIS=1, Z_AXIS=2;
		
	/**
	 * Returns the orientation of a Cube.
	 * If a Cube has the length 1 on all sides, returns randomly 0, 1 or 2
	 * @param cube
	 * @return
	 */
	public static int getOrientation(Cube cube) {
		if (cube.height==1&&cube.depth==1) {
			return X_AXIS;
		} else if (cube.width==1&&cube.depth==1) {
			return Y_AXIS;
		} else if (cube.width==1&&cube.height==1) {
			return Z_AXIS;
		} else {
			boolean xSmallest = (cube.x<=cube.y && cube.x<=cube.z);
			boolean ySmallest = (cube.y<=cube.x && cube.y<=cube.z);
			//boolean zSmallest = (cube.z<=cube.x && cube.z<=cube.y);
			if (xSmallest) return 0;
			else if (ySmallest) return 1;
			else return 2;
		}
	}
	
	/**
	 * Places and returns a new <code>Cube</code> object.
	 * @see #createAndPlaceCube(CubesEclipse, int, int, int, int, int, int, boolean[])
	 * @param parent
	 * @param cube
	 * @param occupied the occupied array
	 * @return the new <code>Cube</code> or <code>null</code> if out of bounds or occupied.
	 */
	public static Cube placeCube(
			CubesEclipse parent, Cube cube, 
			boolean[] occupied, int newColor) {
		cube.parent = parent;
		cube.color = newColor;
		int addResult = cube.placeInSpace(occupied, newColor);
		if (addResult==OK) {
			return cube;
		}
		else {
			return null;
		}
	}
	
	/**
	 * Creates a new random <code>Cube</code> in the defined space 
	 * with a length ranging <code>minLength..maxLength</code>
	 * @param spaceWidth
	 * @param spaceHeight
	 * @param spaceDepth
	 * @param minLength
	 * @param maxLength
	 * @return a <code>Cube</code>
	 */
	public static Cube randomCube(
			int spaceWidth, int spaceHeight, int spaceDepth, 
			int minLength, int maxLength,
			int minAge, int maxAge) 
	{
		
		int age = minAge+rnd(maxAge-minAge);
		int orientation = rnd(3);
		if (orientation==X_AXIS) {			
			int max = Math.min(spaceWidth, maxLength);
			int w = minLength + rnd(max-minLength);
			int h = 1;
			int d = 1;
			int x = rnd(spaceWidth-w);
			int y = rnd(spaceHeight);
			int z = rnd(spaceDepth);
			return new Cube(x,y,z,w,h,d,age);
		} else if (orientation==Y_AXIS) {
			int max = Math.min(spaceHeight, maxLength);
			int w = 1;
			int h = minLength + rnd(max-minLength);
			int d = 1;
			int x = rnd(spaceWidth);
			int y = rnd(spaceHeight-h);
			int z = rnd(spaceDepth);
			return new Cube(x,y,z,w,h,d,age);
		} else if (orientation==Z_AXIS) {
			int max = Math.min(spaceDepth, maxLength);
			int w = 1;
			int h = 1;
			int d = minLength + rnd(max-minLength);
			int x = rnd(spaceWidth);
			int y = rnd(spaceHeight);
			int z = rnd(spaceDepth-d);
			return new Cube(x,y,z,w,h,d,age);
		} else {
			throw new RuntimeException("orientation not 0 1 or 2... very strange!");
		}
	}
	
	/**
	 * Creates a new cube touching the parentCube
	 * @param spaceWidth
	 * @param spaceHeight
	 * @param spaceDepth
	 * @param maxLength
	 * @param parentCube
	 * @return
	 */
	public static Cube randomCubeTouching(
			int spaceWidth, int spaceHeight, int spaceDepth, 
			int minLength, int maxLength, 
			int minAge, int maxAge,
			Cube parentCube) {
		/*
		 * Create random cube
		 */
		Cube cube = randomCube(spaceWidth, spaceHeight, spaceDepth, minLength, maxLength, minAge, maxAge);
		int orientation = Cube.getOrientation(cube);
		
		/*
		 * Get a random point in the parent cube
		 */
		int px = parentCube.x + rnd(parentCube.width) - 1;
		int py = parentCube.y + rnd(parentCube.height) - 1;
		int pz = parentCube.z + rnd(parentCube.depth) - 1;
		
		/*
		 * Get a random point on the new cube;
		 */
		int x = rnd(cube.width) - 1;
		int y = rnd(cube.height) - 1;
		int z = rnd(cube.depth) - 1;
		
		/*
		 * Overlay new cube on parent cube position
		 */
		cube.x = px-x;
		cube.y = py-y;
		cube.z = pz-z;
		
		/*
		 * Move the new cube depending on its orientation
		 */
		if (orientation==X_AXIS) {
			if (rnd(2)==0) {
				cube.z += rndPM();
			} else {
				cube.y += rndPM();
			}
		}
		else if (orientation==Y_AXIS) {
			if (rnd(2)==0) {
				cube.x += rndPM();
			} else {
				cube.z += rndPM();
			}
		}
		else if (orientation==Z_AXIS) {
			if (rnd(2)==0) {
				cube.x += rndPM();
			} else {
				cube.y += rndPM();
			}
		}
		
		return cube;
	}
	
	/**
	 * Same as <code>(int)(n*Math.random())</code>
	 * @param n
	 * @return
	 */
	private static int rnd(int n) {
		return (int)(n*Math.random());
	}
	
	/**
	 * Returns either -1 or 1;
	 * @return
	 */
	private static int rndPM() {
		return Math.random()<0.5?-1:1;
	}
	
	
	
	
	/**
	 * Reference to parent PApplet object;
	 */
	private CubesEclipse parent;
	/**
	 * 'top', 'left', 'back' corner of cube
	 */
	private int x, y, z;
	
	/**
	 * Dimensions of cube in SPACE units
	 */
	private int width, height, depth;
	
	/**
	 * Number of children
	 */
	private int nrOfChildren = 0;
	
	/**
	 * Number of birth tries
	 */
	private int birthTries = 0;
	
	private final int startAge;
	
	/**
	 * Age
	 */
	private int lifeTimeRemaining = 0;
	
	/**
	 * Indexes of SPACE that this cube occupies
	 */
	private Integer[] spaceIndexes;
	
	/**
	 * The color of the cube
	 */
	private int color;
	
	/**
	 * Creates a new Cube at x,y,z with the supplied width, height, depth and age
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param depth
	 * @param age
	 */
	private Cube(int x, int y, int z, int width, int height, int depth, int age) {
		this.x=x;
		this.y=y;
		this.z=z;
		this.width=width;
		this.height=height;
		this.depth=depth;
		this.lifeTimeRemaining=age;
		startAge = age;
	}

	public boolean died() {
		return lifeTimeRemaining<=0;
	}
	
	public float getLifePercentage() {
		return 1-(float)lifeTimeRemaining/startAge;
	}
	
	/*public void draw() {
		float rr = parent.red(color);
		float gg = parent.green(color);
		float bb = parent.blue(color);
		
		float timeRemaining = (float)lifeTimeRemaining/startAge;
		// timeRemaining: 1.0	up		bot		0
		//     to
		// alpha:			0   1  		1   	0
		float up = 0.7f;
		float bot = 0.3f;
		
		float alpha = 0;
		if (timeRemaining>=up) {
			alpha = 1-(timeRemaining-up)/(1-up);
		} else if (timeRemaining>=bot){
			alpha = 1;
		} else {
			alpha = timeRemaining/bot;
		}
		
		
		//int fillColor = parent.color(r,g,b,alpha*255);
		parent.colorMode(PApplet.RGB,255);
		int fillColor = parent.color(rr,gg,bb);
		
		float hu = parent.hue(fillColor);
		float sa = parent.saturation(fillColor);
		float br = parent.brightness(fillColor);
		sa *= alpha;
		parent.colorMode(PApplet.HSB,255);
		fillColor = parent.color(hu,sa,br,alpha*255);
		parent.fill(fillColor);
		parent.colorMode(PApplet.RGB,255);
		parent.stroke(0,0,0,alpha*255);
		
		parent.drawCube(this);
	}*/

	public int getBirthTries() {
		return birthTries;
	}

	public int getColor() {
		return color;
	}

	public int getDepth() {
		return depth;
	}
	
	public int getHeight() {
		return height;
	}

	public int getNrOfChildren() {
		return nrOfChildren;
	}
	
	public Integer[] getSpaceIndexes() {
		return spaceIndexes;
	}

	public int getWidth() {
		return width;
	}

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	public void increaseAge() {
		lifeTimeRemaining--;
	}

	public void increaseBirthTries() {
		birthTries++;
	}

	public void increaseNrOfChildren() {
		nrOfChildren++;
	}

	/**
	 * Sets the occupied array to <code>false</code> at the position where the <code>Cube</code> is.
	 * <p>
	 * If the <code>Cube</code> is anywhere out of bounds, does not set the array and returns {@value Cube#OUT_OF_BOUNDS}.
	 * <p>
	 * If the <code>Cube</code> is trying to occupy an occupied space, does not set the array and return {@link Cube#OCCUPIED}
	 * @param occupied the occupied array to set
	 * @return
	 */
	private int placeInSpace(boolean[] occupied, int newColor) {
		
		ArrayList<Integer> indexesToSet = new ArrayList<Integer>();
		for (int zz=0;zz<depth;zz++) {
			for (int yy=0;yy<height;yy++) {
				for (int xx=0;xx<width;xx++) {
					int zzz = z+zz;
					int yyy = y+yy;
					int xxx = x+xx;
					if (	xxx<0||xxx>=parent.settings.getSpaceWidth()
							||yyy<0||yyy>=parent.settings.getSpaceHeight()
							||zzz<0||zzz>=parent.settings.getSpaceDepth()) {
						return OUT_OF_BOUNDS;
					} else {
						int index = zzz*parent.zDim + yyy*parent.yDim + xxx;
						if (occupied[index]) {
							return OCCUPIED;
						}
						indexesToSet.add(new Integer(index));
					}
				}
			}
		}
		
		this.spaceIndexes = indexesToSet.toArray(new Integer[0]);
		for (int i=0;i<indexesToSet.size();i++) {
			int ii = indexesToSet.get(i);
			occupied[ii] = true;
		}
		
		return OK;
	}

	public void setSpaceIndexes(Integer[] spaceIndexes) {
		this.spaceIndexes = spaceIndexes;
	}

	public String toString() {
		return "Cube ["+width+","+height+","+depth+"] at ["+x+","+y+","+z+"]";
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setZ(int z) {
		this.z = z;
	}

	

	
	
}
