package graf.cubes;

import graf.cubes.settings.Preset01;
import graf.cubes.settings.Preset02;
import graf.cubes.settings.Settings;

import java.util.ArrayList;

import javax.media.opengl.GL;

import processing.core.PApplet;
import processing.core.PVector;
import processing.opengl.PGraphicsOpenGL;

public class CubesEclipse extends PApplet {

	private static final long serialVersionUID = -8820715366194276155L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.cubes.CubesEclipse"});
	}
	
	//final Settings settings = Settings.loadObject("E:\\Cubes\\CubesRenderPreset01\\_settings.obj");
	final Settings settings = new Preset02();
	
	
	/**
	 * 3D Space width (SPACE_WIDTH * SPACE_TO_SCREEN_FAC)
	 */
	private final float spaceWidth = settings.getSpaceWidth()*settings.getSpaceToScreenFactor();
	/**
	 * 3D Space height (SPACE_HEIGHT*SPACE_TO_SCREEN_FAC)
	 */
	private final float spaceHeight = settings.getSpaceHeight()*settings.getSpaceToScreenFactor();
	/**
	 * 3D Space depth (SPACE_DEPTH*SPACE_TO_SCREEN_FAC)
	 */
	private final float spaceDepth = settings.getSpaceDepth()*settings.getSpaceToScreenFactor();
	
	/**
	 * Boolean array that keeps track if a cube in space is occupied.
	 * This array is a 1-dimensional array actually representing 3 dimensions.
	 * Its dimensions are {@link #SPACE_WIDTH} by {@link #SPACE_HEIGHT} by {@link #SPACE_DEPTH}
	 */
	private final boolean[] occupied = new boolean[settings.getSpaceWidth()*settings.getSpaceHeight()*settings.getSpaceDepth()];
	
	/**
	 * used for converting y-coordinate to the 1-dimensional array coordinate of {@link #occupied}
	 */
	final int yDim = settings.getSpaceWidth();
	/**
	 * used for converting z-coordinate to the 1-dimensional array coordinate of {@link #occupied}
	 */
	final int zDim = settings.getSpaceWidth()*settings.getSpaceHeight();
	
	/**
	 * Double-click time in milliseconds
	 */
	private final int DOUBLE_CLICK_TIME = 200;
	
	/**
	 * Center of scene in screen coordinates
	 */
	private final PVector centerOfScene = new PVector(
				spaceWidth*0.5f,
				spaceHeight*0.5f,
				spaceDepth*0.5f
				);
	
	private final PVector cameraOffset = new PVector(
			0,
			0,
			settings.getCameraDistanceFactor()*
			settings.getSpaceToScreenFactor()*
			Math.max(Math.max(settings.getSpaceWidth(), settings.getSpaceHeight()),settings.getSpaceDepth()));
	
	
	
	/**
	 * Cube array
	 */
	private final ArrayList<Cube> cubeList = new ArrayList<Cube>(settings.getCubeMaximum());
	
	/**
	 * last millis() when mouse was pressed. Used for detecting double click.
	 */
	private int lastMousePressed = 0;

	private PVector cameraPosition = new PVector();
	private PVector cameraTarget = PVector.mult(centerOfScene, 1);

	private boolean saveSettings = false;
	private boolean saveFrames = false;
	private String savePath = "E:\\Cubes\\CubesRenderPreset02\\";
	
	
	
	/**
	 * Setup Main
	 */
	public void setup() {
		size(1024,576,OPENGL);
		//System.out.println(settings.toString());
		/*PGraphicsOpenGL pgl = (PGraphicsOpenGL) g;  // g may change
		GL gl = pgl.beginGL();
		gl.glMatrixMode(GL.GL_PROJECTION);
	    gl.glLoadIdentity();
	    gl.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);*/
	    

		/*PGraphicsOpenGL pgl = (PGraphicsOpenGL) g;  // g may change
		GL gl = pgl.beginGL();
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();*/
		//pgl.glu.gluOrtho2D(0.0, width, 0.0, height);
		smooth();
		initialize();
		
		/*PGraphicsOpenGL pgl = (PGraphicsOpenGL) g;
		GL gl = pgl.beginGL();
		gl.glEnable(GL.GL_LIGHTING);
		gl.glEnable(GL.GL_LIGHT0);
		pgl.endGL();*/
		
		//frameRate(3);
		frameRate(25);
	}
	
	public void keyPressed() {
		/*if (frameRate>20) {
			frameRate(3);
		} else {
			frameRate(25);
		}*/
	}
	
	private void initialize() {
		
		for (int i=0;i<occupied.length;i++) {
			occupied[i]=false;
		}
		
		cubeList.clear();
		while (cubeList.size()<1) {
			Cube c = Cube.randomCube(
					settings.getSpaceWidth(), 
					settings.getSpaceHeight(), 
					settings.getSpaceDepth(),
					settings.getCubeLengthMinimum(),
					settings.getCubeLengthMaximum(),
					settings.getAgeMinimum(), 
					settings.getAgeMaximum());
			Cube cube = Cube.placeCube(this, c, occupied, 0xff888888);
			if (cube!=null) {
				cubeList.add(cube);
			}
		}
	}
	
	private void doubleClick() {
		initialize();
	}
	
	public void mousePressed() {
		if (mouseButton==LEFT) {
			int ms = millis();
			int timePassed = ms-lastMousePressed;
			lastMousePressed = ms;
			if (timePassed<DOUBLE_CLICK_TIME) {
				doubleClick();
			}
		}
	}
	
	
	public void mouseDragged() {
		if (mouseButton==LEFT) {
			cameraOffset.add(pmouseX-mouseX,pmouseY-mouseY,0);
		} else if (mouseButton==RIGHT) {
			cameraOffset.add(0,0,pmouseY-mouseY);
		}
		
	}
	
	private void camera(PVector position, PVector target, float upX, float upY, float upZ) {
		camera(position.x, position.y, position.z, target.x, target.y, target.z, upX, upY, upZ);
	}
	
	
	private void createChildren() {
		/*
		 * Check if any more children wanted
		 */
		if (cubeList.size()<settings.getCubeMaximum()) {
			
			int addedThisFrame = 0;
			
			/*
			 * Go through all existing cubes
			 */
			
			int amountWithoutChildren = cubeList.size();
			
			
			int[] shuffledIndexes = new int[amountWithoutChildren];
			for (int i=0;i<shuffledIndexes.length;i++) {
				shuffledIndexes[i] = i;
			}
			
			if (shuffledIndexes.length>2) {
				int i0, i1;
				for (int i=0;i<shuffledIndexes.length;i++) {
					i0 = (int)(shuffledIndexes.length*Math.random());
					i1 = i0;
					while (i1==i0) {
						i1 = (int)(shuffledIndexes.length*Math.random());
					}
					int tmp = shuffledIndexes[i0];
					shuffledIndexes[i0] = shuffledIndexes[i1];
					shuffledIndexes[i1] = tmp;
				}
			}
			
			for (int i=0;i<amountWithoutChildren;i++) {
			//while (addedThisFrame<MAX_CHILDREN_PER_FRAME && cubeList.size()<CUBE_MAXIMUM) {
				
				//Cube cube = cubeList.get((int)(amountWithoutChildren*Math.random()));
				Cube cube = cubeList.get(shuffledIndexes[i]);
				
				boolean totalAmountSurpassed = (cubeList.size()==settings.getCubeMaximum());
				if (totalAmountSurpassed) {
					return;
				}
				
				/*
				 * Check if the cube needs more children
				 * and did not have enough tries yet
				 */
				boolean canHaveMoreChildren = (cube.getNrOfChildren()<settings.getChildrenPerCubeMaximum());
				boolean enoughTries = (cube.getBirthTries()>=settings.getReproductionTriesMaximum());
				
				/*if (!canHaveMoreChildren || enoughTries) {
					continue;
				}*/
				
				if (canHaveMoreChildren && !enoughTries) {
					
					/*
					 * Make a random cube touching this cube
					 */
					Cube c = Cube.randomCubeTouching(
							settings.getSpaceWidth(), 
							settings.getSpaceHeight(), 
							settings.getSpaceDepth(), 
							settings.getCubeLengthMinimum(), 
							settings.getCubeLengthMaximum(), 
							settings.getAgeMinimum(), 
							settings.getAgeMaximum(), cube);
					
					cube.increaseBirthTries();
					
					int childIndex = cubeList.size();
					if (childIndex<settings.getCubeMaximum() && addedThisFrame<settings.getChildrenPerFrameMaximum()) {
						int sourceColor = cube.getColor();
						sourceColor = morphColor(sourceColor, settings.getColorMorphAmount());
						Cube newCube = Cube.placeCube(this, c, occupied, sourceColor);
						if (newCube!=null) {
							cube.increaseNrOfChildren();
							cubeList.add(newCube);
							addedThisFrame++;
						}
					}
				
				}
				
			}
			
		}
		
		
		
		
	}
	
	
	private int morphColor(int sourceColor, int totalChange) {
		
		//int totalChange = 100;
		
		int r = (int)red(sourceColor);
		int g = (int)green(sourceColor);
		int b = (int)blue(sourceColor);
		
		int dirR = Math.random()<0.5?-1:1;
		int dirG = Math.random()<0.5?-1:1;
		int dirB = Math.random()<0.5?-1:1;
		
		for (int i=0;i<totalChange;i++) {
			int channel = (int)(3*Math.random());
			if (channel==0) {
				r+=dirR;
			} else if (channel==1) {
				g+=dirG;
			} else {
				b+=dirB;
			}
		}
		
		return color(r,g,b);
		
	}

	
	
	
	
	public void draw() {
		
		/*if (frameCount<=100) {
			println(frameCount);
			return;
		}*/
		
		if (cubeList.size()==0) {
			initialize();
		}
		
		//smooth();
		
		//gl.glEnable(GL.GL_POLYGON_SMOOTH);
		
		if (settings.getColorIsBlackAndWhite()) {
			background(toGray(settings.getBackgroundColor()));
		} else {
			background(settings.getBackgroundColor());
		}
		
		//lights();
		//dropCubesDownOne();
		
		
		
		/*
		 * Get average of all occupied positions
		 */
		int count=0;
		PVector averagePos = new PVector();
		int index=0;
		int x, y, z;
		for (z=0;z<settings.getSpaceDepth();z++) {
			for (y=0;y<settings.getSpaceHeight();y++) {
				for (x=0;x<settings.getSpaceWidth();x++) {
					if (occupied[index++]) {
						averagePos.x+=x;
						averagePos.y+=y;
						averagePos.z+=z;
						count++;
					}
				}
			}
		}
		
		/*
		 * Set camera target to average occupied position
		 */
		if (count>0) {
			float fac = settings.getSpaceToScreenFactor()/count;
			averagePos.mult(fac);
			cameraTarget = PVector.add( PVector.mult(averagePos, 0.01f), PVector.mult(cameraTarget, 0.99f) );
		}
		
		/*
		 * Set camera position
		 */		
		cameraPosition.set( PVector.add(centerOfScene,cameraOffset) );
		
		/*
		 * Camera!
		 */
		//beginCamera();
		camera(cameraPosition, cameraTarget, 0, 1, 0);
		/*float fov = 45 * PI/180.0f;
		float cameraZ = (height/2.0f) / tan(fov/2.0f);
		perspective(fov, (float)width/(float)height, 
		            cameraZ/10.0f, cameraZ*10.0f);*/

		
		/*
		 * Light
		 */
		/*pointLight(200, 200, 200, 
				-1*SPACE_WIDTH*SPACE_TO_SCREEN_FAC, 
				-1*SPACE_HEIGHT*SPACE_TO_SCREEN_FAC, 
				3*SPACE_DEPTH*SPACE_TO_SCREEN_FAC);*/
		
		/*
		 * START RAW GL
		 */
		PGraphicsOpenGL pgl = (PGraphicsOpenGL) g;
		GL gl = pgl.beginGL();
		
		gl.glEnable(GL.GL_SMOOTH);
		
		//println(GL.GL_MAX_LIGHTS);
		//gl.glEnable(GL.GL_DEPTH_TEST);
		
		makeLights(gl);
		
		
		
		/*
		 * Draw SPACE
		 */
		/*drawBoxFromCornerAt(gl, 
				0, 0, 0, 
				screenWidth, 
				screenHeight, 
				screenDepth,
				new float[] {1,1,1,0.05f},
				null);*/
		
		/*
		 * Draw cubes
		 */
		drawCubes(gl);
		
		
		for (int i=0;i<20;i++) {
			ageCubes();
			createChildren();
		}

			
			
		pgl.endGL();
		//endCamera();
			
		
		if (saveSettings && frameCount==1) {
			String simpleName = "_"+settings.getClass().getSimpleName();
			settings.saveAsAscii(savePath+simpleName+".java");
			settings.saveAsObject(savePath+simpleName+".obj");
		}
		
		if (saveFrames) {
			
			if (frameCount%100==1) {
				println("saving frames to "+savePath);
			}
			if (frameCount%10==0) {
				println("saving frame nr "+frameCount);
			}
			saveFrame(savePath+"frame_#####.bmp");
		}
	}

	

	/**
	 * @param gl
	 */
	private void makeLights(GL gl) {
		gl.glEnable(GL.GL_LIGHTING);
		gl.glEnable(GL.GL_LIGHT0);
		//gl.glEnable(Gl.GL_)
		float globAmb = 0.15f;
		gl.glLightModelfv(GL.GL_LIGHT_MODEL_AMBIENT, new float[] {globAmb,globAmb,globAmb,1.0f}, 0);
		
		//gl.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, new float[] {amb,amb,amb,1}, 0);
		gl.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, new float[] {1,1,1,1}, 0);
		//gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPECULAR, new float[] {1,1,1,1}, 0);
		gl.glLightfv(
				GL.GL_LIGHT0, 
				GL.GL_POSITION, 
				new float[] {-centerOfScene.x,-3*centerOfScene.y,centerOfScene.z, 0}, 
				0);
		
		//gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPOT_DIRECTION, 
			//	new float[] {0,-1,0}, 0);
	}

	private void ageCubes() {
		for (int i=0;i<cubeList.size();i++) {
			Cube cube = cubeList.get(i);
			cube.increaseAge();
			
			if (cube.died()) {
				Integer[] spaceIndexes = cube.getSpaceIndexes();
				for (int ii=0;ii<spaceIndexes.length;ii++) {
					occupied[spaceIndexes[ii]]=false;
				}
				cubeList.remove(i--);
			}

		}
	}
	
	/**
	 * Draws all cubes
	 */
	private void drawCubes(GL gl) {
		for (int i=0;i<cubeList.size();i++) {
			//cubeList.get(i).draw();
			drawCube(gl,cubeList.get(i));
		}
	}

	
	
	
	private void drawBoxFromCenterAt(
			GL gl, 
			float x, float y, float z, 
			float w, float h, float d,
			float[] strokeColor,
			float[] fillColor) {
		
		float xc = x-w/2;
		float yc = y-h/2;
		float zc = z-d/2;
		
		drawBoxFromCornerAt(gl, xc, yc, zc, w, h, d, strokeColor, fillColor);
	}
	
	/**
	 * Draws box at x,y,z not centered
	 */
	private void drawBoxFromCornerAt(
			GL gl, 
			float x, float y, float z, 
			float w, float h, float d,
			float[] strokeColor,
			float[] fillColor) {
		
		if (gl!=null) {
			gl.glPushMatrix();
			gl.glTranslatef(x,y,z);
			if (strokeColor!=null) {
				if (strokeColor.length==3) {
					//gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT_AND_DIFFUSE, 
						//	new float[] { strokeColor[0],strokeColor[1],strokeColor[2] }, 0);
					//gl.glColor3f(strokeColor[0],strokeColor[1],strokeColor[2]);
				} else if (strokeColor.length==4) {
					//gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT_AND_DIFFUSE, 
						//	new float[] { strokeColor[0],strokeColor[1],strokeColor[2],strokeColor[3] }, 0);
					//gl.glColor4f(strokeColor[0],strokeColor[1],strokeColor[2],strokeColor[3]);
				}
				gl.glBegin(GL.GL_LINE_LOOP);
				gl.glVertex3f(0,     0,     0);
				gl.glVertex3f(w, 0,     0);
				gl.glVertex3f(w, h, 0);
				gl.glVertex3f(0,     h, 0);
				gl.glEnd();
				gl.glBegin(GL.GL_LINE_LOOP);
				gl.glVertex3f(0,     0,     d);
				gl.glVertex3f(w, 0,     d);
				gl.glVertex3f(w, h, d);
				gl.glVertex3f(0,     h, d);
				gl.glEnd();
				
				gl.glBegin(GL.GL_LINES);
				gl.glVertex3f(0,     0,     0);
				gl.glVertex3f(0,     0,     d);
				gl.glVertex3f(w, 0,     0);
				gl.glVertex3f(w, 0,     d);
				gl.glVertex3f(w, h, 0);
				gl.glVertex3f(w, h, d);
				gl.glVertex3f(0,     h, 0);
				gl.glVertex3f(0,     h, d);
				gl.glEnd();
				//gl.glColor3f(0, 0, 0);
			}
			if (fillColor!=null) {
				//if (fillColor.length==3) {
					/*gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT_AND_DIFFUSE, 
							new float[] { strokeColor[0],strokeColor[1],strokeColor[2] }, 0);*/
					//gl.glColor3f(fillColor[0],fillColor[1],fillColor[2]);
					/*gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, 
							new float[] { fillColor[0]/2,fillColor[1]/2,fillColor[2]/2, 1 }, 0);
					gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, 
							new float[] { fillColor[0],fillColor[1],fillColor[2], 1 }, 0);*/
				//} else if (fillColor.length==4) {
					/*gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT_AND_DIFFUSE, 
							new float[] { strokeColor[0],strokeColor[1],strokeColor[2],strokeColor[3] }, 0);*/
					//gl.glColor4f(fillColor[0],fillColor[1],fillColor[2],fillColor[3]);
					/*gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, 
							new float[] { fillColor[0]/2,fillColor[1]/2,fillColor[2]/2,fillColor[3] }, 0);
					gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, 
							new float[] { fillColor[0],fillColor[1],fillColor[2],fillColor[3] }, 0);*/
				//}
				//gl.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, new float[] { 1,1,1,1 }, 0);
				
				gl.glTranslatef(w/2,h/2,d/2);
				for (int f=0;f<6;f++) {
					drawFaceOfBox(gl,f,w/2,h/2,d/2,fillColor);
				}
				//gl.glColor3f(0, 0, 0);
			}
			
			gl.glPopMatrix();
		} else {
			//TODO: Processing routines!
			
		}
	}
	
	private void drawFaceOfBox(GL gl, int faceNr, float x, float y, float z, float[] matColor) {
		gl.glBegin(GL.GL_QUADS);
		if (matColor!=null) {
			float ambFac = 1;
			float[] ambColor = new float[] {matColor[0]*ambFac, matColor[1]*ambFac, matColor[2]*ambFac, 1};
			gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambColor, 0);
			gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, matColor, 0);
			//gl.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, new float[] {0}, 0);
		}
		switch (faceNr) {
			case(0): // back
				gl.glNormal3f(0, 0, -1);
				gl.glVertex3f(-x, -y, -z);
				gl.glVertex3f(x, -y, -z);
				gl.glVertex3f(x, y, -z);
				gl.glVertex3f(-x, y, -z);
				break;
			case(1): // front
				gl.glNormal3f(0, 0, 1);
				gl.glVertex3f(-x, -y, z);
				gl.glVertex3f(x, -y, z);
				gl.glVertex3f(x, y, z);
				gl.glVertex3f(-x, y, z);
				break;
			case(2): // left
				gl.glNormal3f(-1, 0, 0);
				gl.glVertex3f(-x, -y, -z);
				gl.glVertex3f(-x, -y, z);
				gl.glVertex3f(-x, y, z);
				gl.glVertex3f(-x, y, -z);
				break;
			case(3): // right
				gl.glNormal3f(1, 0, 0);
				gl.glVertex3f(x, -y, -z);
				gl.glVertex3f(x, -y, z);
				gl.glVertex3f(x, y, z);
				gl.glVertex3f(x, y, -z);
				break;
			case(4): // top
				gl.glNormal3f(0, -1, 0);
				gl.glVertex3f(-x, -y, -z);
				gl.glVertex3f(-x, -y, z);
				gl.glVertex3f(x, -y, z);
				gl.glVertex3f(x, -y, -z);
				break;
			case(5): // bottom
				gl.glNormal3f(0, 1, 0);
				gl.glVertex3f(-x, y, -z);
				gl.glVertex3f(-x, y, z);
				gl.glVertex3f(x, y, z);
				gl.glVertex3f(x, y, -z);
				break;
			default: break;
		}
		gl.glEnd();
	}

	private void drawCube(GL gl,Cube c) {
		int color = c.getColor();
		float rr = (color>>16&0xff);//red(color);
		float gg = (color>>8&0xff);//green(color);
		float bb = (color&0xff);//blue(color);
		//println(rr+" "+gg+" "+bb);
		
		float timeRemaining = 1 - c.getLifePercentage(); //(float)lifeTimeRemaining/startAge
		// timeRemaining: 1.0	up		bot		0
		//     to
		// alpha:			0   1  		1   	0
		float up = 0.7f;
		float bot = 0.3f;
		
		float alpha = 0;
		if (timeRemaining>=up) {
			alpha = 1-(timeRemaining-up)/(1-up);
			alpha = (float)Math.sqrt(alpha);
		} else if (timeRemaining>=bot){
			alpha = 1;
		} else {
			alpha = timeRemaining/bot;
			alpha = (float)Math.sqrt(alpha);
		}
		float growFac = alpha;
		
		int cornerX = c.getX();
		int cornerY = c.getY();
		int cornerZ = c.getZ();
		float screenCornerX = cornerX * settings.getSpaceToScreenFactor();
		float screenCornerY = cornerY * settings.getSpaceToScreenFactor();
		float screenCornerZ = cornerZ * settings.getSpaceToScreenFactor();
		
		int width = c.getWidth();
		int height = c.getHeight();
		int depth = c.getDepth();
		float screenWidth = width * settings.getSpaceToScreenFactor();
		float screenHeight = height * settings.getSpaceToScreenFactor();
		float screenDepth = depth * settings.getSpaceToScreenFactor();
		
		float screenCenterX = screenCornerX + screenWidth/2;
		float screenCenterY = screenCornerY + screenHeight/2;
		float screenCenterZ = screenCornerZ + screenDepth/2;

		
		float[] fillColor;
		if (settings.getColorIsBlackAndWhite()) {
			float gray = toGray(color);
			fillColor = new float[] {gray/255, gray/255, gray/255, 1.0f};
		} else {
			fillColor = new float[] {rr/255,gg/255,bb/255,1.0f};
		}
		
		float[] strokeColor = null;//new float[] {0.01f,0.01f,0.01f};//rr/2048,gg/2048,bb/2048};
		
		
		if (growFac!=1) {
			int orientation = Cube.getOrientation(c);
			if (orientation==Cube.X_AXIS) {
				drawBoxFromCenterAt(gl, 
						screenCenterX, screenCenterY, screenCenterZ, 
						screenWidth*growFac, screenHeight, screenDepth, 
						strokeColor, fillColor);
			} else if (orientation==Cube.Y_AXIS){
				drawBoxFromCenterAt(gl, 
						screenCenterX, screenCenterY, screenCenterZ, 
						screenWidth, screenHeight*growFac, screenDepth, 
						strokeColor, fillColor);
			} else {
				drawBoxFromCenterAt(gl, 
						screenCenterX, screenCenterY, screenCenterZ, 
						screenWidth, screenHeight, screenDepth*growFac, 
						strokeColor, fillColor);
			}
			
		} else {
			drawBoxFromCenterAt(gl, 
					screenCenterX, screenCenterY, screenCenterZ, 
					screenWidth, screenHeight, screenDepth, 
					strokeColor, fillColor);
		}		
	}
		
	int toGray(int rgb) {
		int gray = ((rgb>>16&0xff)+(rgb>>8&0xff)+(rgb&0xff))/3; 
		return gray;
	}

}
