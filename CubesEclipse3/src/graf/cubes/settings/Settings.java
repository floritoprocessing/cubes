/**
 * 
 */
package graf.cubes.settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * 
 * @author Marcus
 */
public abstract class Settings implements Serializable {
	
	/**
	 * Maximum age
	 */
	public abstract int getAgeMaximum();
	
	
	/**
	 * Minimum age
	 */
	public abstract int getAgeMinimum();
	
	/**
	 * Background color
	 */
	public abstract int getBackgroundColor();
	
	/**
	 * Distance camera to center based on SPACE dimensions
	 */
	public abstract float getCameraDistanceFactor();
	/**
	 * Max children per Cube
	 */
	public abstract int getChildrenPerCubeMaximum();
	/**
	 * Max children added per frame
	 */
	public abstract int getChildrenPerFrameMaximum();//CHILDREN_PER_FRAME_MAXIMUM;
	/**
	 * Render in black and white
	 */
	public abstract boolean getColorIsBlackAndWhite();//COLOR_BLACK_AND_WHITE;
	/**
	 * The amount that the color morphs from parent to child
	 */
	public abstract int getColorMorphAmount();//COLOR_MORPH_AMOUNT;	
	/**
	 * Max length of a <code>Cube</code> to be created
	 */
	public abstract int getCubeLengthMaximum();//CUBE_LENGTH_MAXIMUM;
	/**
	 * Minimum length of a <code>Cube</code>
	 */
	public abstract int getCubeLengthMinimum();//CUBE_LENGTH_MINIMUM;
	/**
	 * Maximum amount of cubes to be created
	 */
	public abstract int getCubeMaximum();//CUBE_MAXIMUM;
	/**
	 * Max tries of reproduction
	 */
	public abstract int getReproductionTriesMaximum();//REPRODUCTION_TRIES_MAXIMUM;
	/**
	 * The space is divided in cubes. This is its d
	 */
	public abstract int getSpaceDepth();//SPACE_DEPTH;
	/**
	 * The space is divided in cubes. This is its height
	 */
	public abstract int getSpaceHeight();//SPACE_HEIGHT;
	/**
	 * The factor to scale the <code>occupied</code> to the screen coordinates
	 */
	public abstract float getSpaceToScreenFactor();//SPACE_TO_SCREEN_FAC;
	/**
	 * The space is divided in cubes. This is its width
	 */
	public abstract int getSpaceWidth();//SPACE_WIDTH;
	
	public Settings() {
		//toStrings();
		//System.out.println("settings");
		//System.out.println(toString());
	}
	
	public String toString() {
		String[] lines = toStrings();
		String nl = System.getProperty("line.separator");
		StringBuilder sb = new StringBuilder();
		for (String line:lines) {
			sb.append(line+nl);
		}
		return sb.toString();
	}
	
	public String[] toStrings() {
		ArrayList<String> lines = new ArrayList<String>();
		
		Class<? extends Settings> cl = this.getClass();
		
		String packageName = cl.getPackage().getName();
		lines.add("/*");
		lines.add("*");
		lines.add("* Created with CubesEclipse3");
		lines.add("*");
		lines.add("*/");
		lines.add("package "+packageName+";");
		lines.add("");
		String className = cl.getSimpleName();
		String superName = cl.getSuperclass().getSimpleName();
		lines.add("public class "+className+" extends "+superName+" {");
		
		Method[] methods = cl.getDeclaredMethods();
		
		
		
		//cl.get
		for (Method method:methods) {
			String methodName = method.getName();
			String type = method.getGenericReturnType().toString();
			lines.add("\tpublic "+type+" "+methodName+"() {");
			try {
				Object[] objects = null;
				Object returnValue = method.invoke(this, objects);
				String floatString = returnValue.getClass().equals(Float.class)?"f":"";
				String value = returnValue.toString();
				if (returnValue.getClass().equals(Integer.class)&& methodName.toLowerCase().endsWith("color")) {
					value = "0x"+Integer.toHexString(new Integer(value));
				}
				lines.add("\t\treturn "+value+floatString+";");
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			lines.add("\t}");
		}
		
		lines.add("}");
		return lines.toArray(new String[0]);
	}
	
	
	public void saveAsAscii(String path) {
		System.out.println("Saving as ascii to "+path);
		File file = new File(path);
		
		file.getParentFile().mkdirs();
		FileOutputStream fos = null;
		String[] lines = toString().split(System.getProperty("line.separator"));
		try {
			fos = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(fos);
			for (String line:lines) {
				pw.println(line);
			}
			pw.flush();
			pw.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void saveAsObject(String path) {
		System.out.println("Saving as object to "+path);
		File file = new File(path);
		file.getParentFile().mkdirs();
		try {
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Settings loadObject(String path) {
		File file = new File(path);
		Settings settings = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			settings = (Settings)ois.readObject();
			ois.close();
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return settings;
	}

}
