package graf.cubes.settings;

public class Preset02 extends Settings 
{
	private static final long serialVersionUID = -2673520354040491475L;
	
	public int getSpaceWidth() {
		return 100;
	}
	public float getSpaceToScreenFactor() {
		return 10.0f;
	}
	public int getSpaceHeight() {
		return 60;
	}
	public int getSpaceDepth() {
		return 100;
	}
	public float getCameraDistanceFactor() {
		return 1.0f;
	}
	public int getCubeMaximum() {
		return 800;
	}
	public int getCubeLengthMinimum() {
		return 2;
	}
	public int getCubeLengthMaximum() {
		return 6;
	}
	public int getAgeMinimum() {
		return 300;
	}
	public int getAgeMaximum() {
		return 350;
	}
	public int getChildrenPerCubeMaximum() {
		return 3;
	}
	public int getReproductionTriesMaximum() {
		return 8;
	}
	public int getChildrenPerFrameMaximum() {
		return 3;
	}
	public int getColorMorphAmount() {
		return 40;
	}
	public boolean getColorIsBlackAndWhite() {
		return false;
	}
	public int getBackgroundColor() {
		return 0xff000000;
	}
}
