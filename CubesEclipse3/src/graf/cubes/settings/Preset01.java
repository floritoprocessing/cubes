/**
 * 
 */
package graf.cubes.settings;

/**
 * @author Marcus
 *
 */
public class Preset01 extends Settings {

	private static final long serialVersionUID = -608951542551016250L;
	
	int AGE_MAXIMUM = 350;
	int AGE_MINIMUM = 300;
	int BACKGROUND_COLOR = 0xff000000;
	float CAMERA_DISTANCE_FAC = 0.5f;
	int CHILDREN_PER_CUBE_MAXIMUM = 3;
	int CHILDREN_PER_FRAME_MAXIMUM = 3;
	boolean COLOR_BLACK_AND_WHITE = false;
	int COLOR_MORPH_AMOUNT = 40;
	int CUBE_LENGTH_MAXIMUM = 6;
	int CUBE_LENGTH_MINIMUM = 2;
	int CUBE_MAXIMUM = 800;
	int REPRODUCTION_TRIES_MAXIMUM = 8;
	int SPACE_DEPTH = 100;
	int SPACE_HEIGHT = 60;
	float SPACE_TO_SCREEN_FAC = 10.0f;
	int SPACE_WIDTH = 100;
	
	public Preset01() {
	}

	public int getAgeMaximum() {
		return AGE_MAXIMUM;
	}

	public int getAgeMinimum() {
		return AGE_MINIMUM;
	}

	public int getBackgroundColor() {
		return BACKGROUND_COLOR;
	}

	public float getCameraDistanceFactor() {
		return CAMERA_DISTANCE_FAC;
	}

	public int getChildrenPerCubeMaximum() {
		return CHILDREN_PER_CUBE_MAXIMUM;
	}

	public int getChildrenPerFrameMaximum() {
		return CHILDREN_PER_FRAME_MAXIMUM;
	}

	public boolean getColorIsBlackAndWhite() {
		return COLOR_BLACK_AND_WHITE;
	}

	public int getColorMorphAmount() {
		return COLOR_MORPH_AMOUNT;
	}

	public int getCubeLengthMaximum() {
		return CUBE_LENGTH_MAXIMUM;
	}

	public int getCubeLengthMinimum() {
		return CUBE_LENGTH_MINIMUM;
	}

	public int getCubeMaximum() {
		return CUBE_MAXIMUM;
	}

	public int getReproductionTriesMaximum() {
		return REPRODUCTION_TRIES_MAXIMUM;
	}

	public int getSpaceDepth() {
		return SPACE_DEPTH;
	}

	public int getSpaceHeight() {
		return SPACE_HEIGHT;
	}

	public float getSpaceToScreenFactor() {
		return SPACE_TO_SCREEN_FAC;
	}

	public int getSpaceWidth() {
		return SPACE_WIDTH;
	}
	
}
