import java.util.ArrayList;
import java.util.Arrays;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PMatrix3D;
import processing.core.PVector;


public class Cubes extends PApplet {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"Cubes"});
	}
	
	int WIDTH = 1280;
	int HEIGHT = 720;
	int far = -2000;
	int LENGTH = 800;
	
	PVector[] lightPos = new PVector[3];
	PVector[] targetLightPos = new PVector[3];
	int[] nextLightInit = new int[] {1,1,1};
	int[] col = new int[] { color(255,128,64), color(255,0,12), color(255,90,90) };
	
	long times = 0;
	Box[] b;
	
	public void settings() {
		size(WIDTH,HEIGHT,P3D);
	}
	public void setup() {
		
		smooth();
		b = new Box[10000];
		for (int i=0;i<b.length;i++) {
			//b[i] = new Box(random(-WIDTH,WIDTH),random(-HEIGHT,HEIGHT),random(far,0),5,LENGTH,5);
			b[i] = new Box(15,LENGTH,15);
			
			float maxR= 500;
			float radius = (float)(Math.sqrt(Math.random())*maxR);
			float rd = random(TWO_PI);
			float x = radius*cos(rd);
			float z = radius*sin(rd);
			b[i].setPosition(new PVector(x,0,z));
			
			PVector rndPos = new PVector(random(WIDTH),random(HEIGHT),random(WIDTH));
			PVector mov = PVector.sub(rndPos, new PVector(WIDTH/2,HEIGHT,WIDTH/2));
			mov.normalize();
			mov.x *= 2;
			mov.y *= 4;
			mov.z *= 2;
			mov.mult(random(10,40));
			b[i].setMovement(mov);
			
			PMatrix3D orientation = new PMatrix3D();
			//orientation.rotateX((float)(Math.random()*Math.PI*2));
			//orientation.rotateY((float)(Math.random()*Math.PI*2));
			//orientation.rotateZ((float)(Math.random()*Math.PI*2));
			//orientation.rotateX(-radians(85));
			b[i].setOrientation(orientation);
			
			PMatrix3D rotation = new PMatrix3D();
			rotation.rotateX(random(0.005f,0.05f)*(Math.random()<0.5?-1:1));
			rotation.rotateY(random(0.005f,0.05f)*(Math.random()<0.5?-1:1));
			rotation.rotateZ(random(0.005f,0.05f)*(Math.random()<0.5?-1:1));
			b[i].setRotation(rotation);
		}
		
		
		for (int i=0;i<lightPos.length;i++) {
			lightPos[i] = new PVector(0,0,far/2);
			targetLightPos[i] = new PVector();
			newTarget(i);
		}
		
	}
	
	private void newTarget(int index) {
		targetLightPos[index].set(
				random(-2*WIDTH,2*WIDTH),
				random(-2*HEIGHT,2*HEIGHT),
				random(2*far,-far));
	}
	
	public void draw() {
		drawAll();
		//saveFrame("Cubes_####.bmp");
	}
	
	public void keyPressed() {
		if (key=='s') {
			saveFrame("Cubes_###.jpg");
		}
	}
		
	private void drawAll() {
		background(0);
		pushMatrix();
		//translate(width/2,height/2);
		
		//float freq = 0.01f;
		//float rd = freq * frameCount/25.0f * TWO_PI;
		float upX = 0;//sin(rd);
		float upY = 1;//cos(rd);
		
		//5000 - frameCount*50
		if (frameCount<50) {
			float zMin = 200;
			float zMax = 5000;
			float perc = frameCount/50.0f;
			perc = sin(perc*HALF_PI);
			float z = zMin + perc*(zMax-zMin);
			camera(0, 0, frameCount*100, 0, 0, far, upX, upY, 0);
		} else {
			camera(0, 0, 5000, 0, 0, far, upX, upY, 0);
		}
				
		for (int i=0;i<lightPos.length;i++) {
		
			float x = lightPos[i].x;
			float y = lightPos[i].y;
			float z = lightPos[i].z;
			
			fill(col[i]);
			noStroke();
			pushMatrix();
			translate(x,y,z);
			sphere(20);
			popMatrix();
			
		}
		
		for (int i=0;i<lightPos.length;i++) {
			float x = lightPos[i].x;
			float y = lightPos[i].y;
			float z = lightPos[i].z;
			pointLight(red(col[i]), green(col[i]), blue(col[i]), x, y, z);
		}
		
		directionalLight(20, 20, 20, 0, 0, -1);
		
		
		fill(255,255,255);
		noStroke();
		int ms = millis();
		for (int i=0;i<b.length;i++) {
			b[i].updateAndDraw(this);
		}
		int ti = millis()-ms;
		times += ti;
		
		
		println("frame: "+frameCount+": "+ti+" (average: "+(times/frameCount)+")");
		
		if (frameCount>50) {
			for (int i=0;i<b.length;i++) {
				b[i].updateOrientation();
				b[i].move();
				b[i].getMovement().add(new PVector(0,1,0));
				b[i].getMovement().mult(0.99f);
			}
		}
		
		for (int i=0;i<lightPos.length;i++) {
			lightPos[i] = PVector.add( 
					PVector.mult(lightPos[i],0.9f), 
					PVector.mult(targetLightPos[i],0.1f) );
			if (frameCount==nextLightInit[i]) {
				newTarget(i);
				nextLightInit[i] = frameCount+(int)random(10,50);
			}
		}
		popMatrix();
	}

}
