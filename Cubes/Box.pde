class Box {

  private PVector pos = new PVector();
  private PVector mov = new PVector();
  //private float xRot=0, yRot=0, zRot=0;
  private PMatrix orientation;
  private PMatrix rotation;
  private float width, height, depth;

  public Box(float width, float height, float depth) {
    this(0,0,0,width,height,depth);
  }

  public Box(float x, float y, float z, float width, float height, float depth) {
    pos = new PVector(x,y,z);
    orientation = new PMatrix3D();
    rotation = new PMatrix3D();
    rotation.rotateX(0.1f);
    this.width = width;
    this.height = height;
    this.depth = depth;
  }

  public void setPosition(PVector pos) {
    this.pos.set(pos);
  }

  public PVector getMovement() {
    return mov;
  }

  public void setMovement(PVector mov) {
    this.mov.set(mov);
  }

  public void setOrientation(PMatrix3D orientation) {
    this.orientation = new PMatrix3D(orientation);
  }

  public void setRotation(PMatrix3D rotation) {
    this.rotation = new PMatrix3D(rotation);
  }

  /*public void rotate(float rotX, float rotY, float rotZ) {
   		xRot += rotX;
   		yRot += rotY;
   		zRot += rotZ;
   	}*/

  public void move() {
    pos.add(mov);
  }

  public void updateOrientation() {
    orientation.apply(rotation);
  }

  public void updateAndDraw(PApplet pa) {



    pa.pushMatrix();
    pa.translate(pos.x,pos.y,pos.z);
    pa.applyMatrix(orientation);
    /*pa.rotateX(xRot);
     		pa.rotateY(yRot);
     		pa.rotateZ(zRot);*/
    pa.box(width,height,depth);
    pa.popMatrix();
  }

}

