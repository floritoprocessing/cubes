package graf.cubes.settings;

public class Preset02 extends Settings 
{
	private static final long serialVersionUID = -2673520354040491475L;
	
	public int getAgeMinimum() {
		return 75;
	}
	public int getAgeMaximum() {
		return 100;
	}
	
	public int getAgingPerFrameIterations() {
		return 10; //20
	}
	public int getBackgroundColor() {
		return 0xffe0e0e0;
	}
	
	public float getCameraXFactor() {
		return 0;
	}
	public float getCameraYFactor() {
		return -0.8f;
	}
	public float getCameraZFactor() {
		return 0.8f;
	}
	
	public int getChildrenPerCubeMaximum() {
		return 5;
	}
	public int getChildrenPerFrameMaximum() {
		return 20; 
	}
	public boolean getColorIsBlackAndWhite() {
		return true;
	}
	public int getColorMorphAmount() {
		return 30; //40
	}
	
	public int getCubeLengthMinimum() {
		return 4;
	}
	public int getCubeLengthMaximum() {
		return 8;
	}
	
	public int getCubeMaximum() {
		return 5000;
	}
	
	public int getReproductionExclusionFrameMinimum() {
		return 13;
	}
	public int getReproductionExclusionFrameMaximum() {
		return 25;
	}
	
	public int getReproductionTriesMaximum() {
		return 8;
	}
	public int getSpaceDepth() {
		return 100;
	}
	public int getSpaceHeight() {
		return 60;
	}
	public float getSpaceToScreenFactor() {
		return 10.0f;
	}
	public int getSpaceWidth() {
		return 100;
	}
	
}
