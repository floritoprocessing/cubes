class Box {

  private PVector pos = new PVector();
  private PVector mov = new PVector();

  private PMatrix orientation;
  private float width, height, depth;
  private int axis;
  private float shortSide, longSide;
  
  private int children = 0;
  private final int CHILDREN_MAX = 3;

  public Box(float x, float y, float z, int axis, float shortSide, float longSide) {
    pos = new PVector(x,y,z);
    orientation = new PMatrix3D();
    
    this.axis = axis;
    this.shortSide = shortSide;
    this.longSide = longSide;
    width = shortSide;
    height = shortSide;
    depth = shortSide;
    
    if (axis==0) {
      //X
      width = longSide;
    } else if (axis==1) {
      //Y
      height = longSide;
    } else {
      //Z
      depth = longSide;
    }
    this.width = width;
    this.height = height;
    this.depth = depth;
  }
  
  public boolean hasChild() {
    return children>CHILDREN_MAX;//hasChild;
  }
  
  private boolean rnd() {
    return Math.random()<0.5;
  }
  
  public Box createChild() {
    
    int newAxis=-1;
    float nx = pos.x;
    float ny = pos.y;
    float nz = pos.z;
    int maxFac = Math.min(3,(int)(longSide/shortSide/2)-1);
    
    if (axis==0) {
      newAxis = rnd()?1:2;
      if (newAxis==1) {
        nz += 2 * (rnd()?-shortSide:shortSide);
        nx += 2 * (rnd()?-1:1) * random(0,maxFac) * shortSide;
      } else {
        ny += 2 * (rnd()?-shortSide:shortSide);
        nx += 2 * (rnd()?-1:1) * random(0,maxFac) * shortSide;
      }
    }
    
    else if (axis==1) {
      newAxis = rnd()?0:2;
      if (newAxis==0) {
        nz += 2 * (rnd()?-shortSide:shortSide);
        ny += 2 * (rnd()?-1:1) * random(0,maxFac) * shortSide;
      } else {
        nx += 2 * (rnd()?-shortSide:shortSide);
        ny += 2 * (rnd()?-1:1) * random(0,maxFac) * shortSide;
      }
    }
    
    else if (axis==2) {
      newAxis = rnd()?0:1;
      if (newAxis==0) {
        ny += 2 * (rnd()?-shortSide:shortSide);
        nz += 2 * (rnd()?-1:1) * random(0,maxFac) * shortSide;
      } else {
        nx += 2 * (rnd()?-shortSide:shortSide);
        nz += 2 * (rnd()?-1:1) * random(0,maxFac) * shortSide;
      }
    }
    
    if (newAxis!=-1) {
      //hasChild = true;
      children++;
      Box out = new Box(nx,ny,nz,newAxis,shortSide,longSide);
      return out;      
    }
    
    
    
    return null;
  }

  public void setPosition(PVector pos) {
    this.pos.set(pos);
  }

  public PVector getMovement() {
    return mov;
  }

  public void setMovement(PVector mov) {
    this.mov.set(mov);
  }

  public void setOrientation(PMatrix3D orientation) {
    this.orientation = new PMatrix3D(orientation);
  }

  public void integrate(PVector acc, float time) {
    if (acc==null) {
      acc = new PVector();
    }
    float ti2 = 0.5*time*time;
    
    float nx = pos.x + mov.x*time + acc.x*ti2;
    float ny = pos.y + mov.y*time + acc.y*ti2;
    float nz = pos.z + mov.z*time + acc.z*ti2;
    
    mov.x = (nx-pos.x)/time;
    mov.y = (ny-pos.y)/time;
    mov.z = (nz-pos.z)/time;
    
    pos.set(nx,ny,nz);
  }

  public void updateOrientation(float rotX, float rotY, float rotZ, float time) {
    PMatrix3D rotation = new PMatrix3D();
    rotation.rotateX(rotX*time);
    rotation.rotateY(rotY*time);
    rotation.rotateZ(rotZ*time);
    orientation.apply(rotation);
  }

  public void updateAndDraw(PApplet pa) {
    pa.pushMatrix();
    pa.translate(pos.x,pos.y,pos.z);
    pa.applyMatrix(orientation);
    pa.box(width,height,depth);
    pa.popMatrix();
  }

}

