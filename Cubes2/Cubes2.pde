import processing.opengl.*;
import processing.video.*;

int WIDTH = 1280;
int HEIGHT = 720;
int MAX_BOXES = 1000;

int preFrame = 0;
long times = 0;
ArrayList boxes;
//MovieMaker mm = null;


void setup() {
  size(1280,720,P3D);
  smooth();
  boxes = new ArrayList(MAX_BOXES);
  //for (int i=0;i<100;i++) {
  //  boxes.add( new Box(random(-width,width), random(-height,height), random(-width,width), (int)random(3), 5, 200));
  //}
  boxes.add( new Box(0,0,0,0, 5, 200) );
}

void draw() {
  background(0);

  camera(0,0,500,0,0,0,0,1,0);
  directionalLight(64,64,64,0,0,-1);
  pointLight(255,40,20,-200,-800,500);

  stroke(0);
  fill(255,255,255);  
  
  int total = boxes.size();
    if (total<MAX_BOXES) {
    ArrayList newChildren = new ArrayList();
    for (int i=0;i<boxes.size();i++) {
      total = boxes.size() + newChildren.size();
      if (total<MAX_BOXES) {
        Box b = (Box)boxes.get(i);    
        if (!b.hasChild()) {
          if (Math.random()<0.3) {
            Box child = b.createChild();
            if (child!=null) {
              newChildren.add(child);
            }
          }
        }
      }
    }
    boxes.addAll(newChildren);
  }
  
  for (int i=0;i<boxes.size();i++) {
    Box b = (Box)boxes.get(i);
    b.updateAndDraw(this);
  }
  
  
  
  //if (mm!=null) {
  //  mm.addFrame();
  //  if (keyPressed&&key=='Q') {
  //    mm.finish();
  //    System.exit(0);
  //  }
  //}
}
