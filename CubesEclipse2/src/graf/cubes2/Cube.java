package graf.cubes2;

import java.util.ArrayList;

public class Cube {

	/**
	 * OK result of the {@link #testSpace(boolean[])} Method
	 */
	private static final int OK = 0;
	
	/**
	 * OCCUPIED result of the {@link #testSpace(boolean[])} Method
	 */
	private static final int OCCUPIED = 1;
	
	/**
	 * OUT_OF_BOUNDS result of the {@link #testSpace(boolean[])} Method
	 */
	private static final int OUT_OF_BOUNDS = 2;
	
	private static final int X_AXIS=0, Y_AXIS=1, Z_AXIS=2;
	
	/**
	 * Creates, places and returns a new <code>Cube</code> object, 
	 * if the space is not occupied and the Cube is not out of bounds
	 * @param parent
	 * @param x the x position
	 * @param y the y position
	 * @param z the z position
	 * @param width the width
	 * @param height the height
	 * @param depth the depth
	 * @param occupied the occupied array
	 * @return the new <code>Cube</code> or <code>null</code> if out of bounds or occupied.
	 */
	/*public static Cube createAndPlaceCube(CubesEclipse2 parent, 
			int x, int y, int z, 
			int width, int height, int depth, 
			boolean[] occupied) {
		
		return placeCube(parent, new Cube(parent, x, y, z, width, height, depth), occupied);
	}*/
	
	/**
	 * Places and returns a new <code>Cube</code> object.
	 * @see #createAndPlaceCube(CubesEclipse2, int, int, int, int, int, int, boolean[])
	 * @param parent
	 * @param cube
	 * @param occupied the occupied array
	 * @return the new <code>Cube</code> or <code>null</code> if out of bounds or occupied.
	 */
	public static Cube placeCube(CubesEclipse2 parent, Cube cube, boolean[] occupied, int[] occupiedIndex, int[] occupiedColor, int newIndex, int newColor) {
		//Cube out = new Cube(parent,x,y,z,width,height,depth);
		cube.parent = parent;
		
		int addResult = cube.testSpace(occupied, occupiedIndex, occupiedColor, newIndex, newColor);
		if (addResult==OK) {
			//System.out.println(newIndex);
			return cube;
		}
		else {
			return null;
		}
	}
	
	public static Cube randomCube(int spaceWidth, int spaceHeight, int spaceDepth, int maxLength) {
		
		int orientation = rnd(3);
		if (orientation==X_AXIS) {
			int max = Math.min(spaceWidth, maxLength);
			int w = 1 + rnd(max-1);
			int h = 1;
			int d = 1;
			int x = rnd(spaceWidth-w);
			int y = rnd(spaceHeight);
			int z = rnd(spaceDepth);
			return new Cube(x,y,z,w,h,d);
		} else if (orientation==Y_AXIS) {
			int max = Math.min(spaceHeight, maxLength);
			int w = 1;
			int h = 1 + rnd(max-1);
			int d = 1;
			int x = rnd(spaceWidth);
			int y = rnd(spaceHeight-h);
			int z = rnd(spaceDepth);
			return new Cube(x,y,z,w,h,d);
		} else if (orientation==Z_AXIS) {
			int max = Math.min(spaceDepth, maxLength);
			int w = 1;
			int h = 1;
			int d = 1 + rnd(max-1);
			int x = rnd(spaceWidth);
			int y = rnd(spaceHeight);
			int z = rnd(spaceDepth-d);
			return new Cube(x,y,z,w,h,d);
		} else {
			throw new RuntimeException("orientation not 0 1 or 2... very strange!");
		}
	}
	
	public static Cube randomCubeTouching(int spaceWidth, int spaceHeight, int spaceDepth, int maxLength, Cube parentCube) {
		/*
		 * Create random cube
		 */
		Cube cube = randomCube(spaceWidth, spaceHeight, spaceDepth, maxLength);
		int orientation = cube.getOrientation(cube);
		
		/*
		 * Get a random point in the parent cube
		 */
		int px = parentCube.x + rnd(parentCube.width);
		int py = parentCube.y + rnd(parentCube.height);
		int pz = parentCube.z + rnd(parentCube.depth);
		
		/*
		 * Get a random point on the new cube;
		 */
		int x = rnd(cube.width);
		int y = rnd(cube.height);
		int z = rnd(cube.depth);
		
		/*
		 * Overlay new cube on parent cube position
		 */
		cube.x = px-x;
		cube.y = py-y;
		cube.z = pz-z;
		
		/*
		 * Move the new cube depending on its orientation
		 */
		if (orientation==X_AXIS) {
			if (rnd(2)==0) {
				cube.z += rndPM();
			} else {
				cube.y += rndPM();
			}
		}
		else if (orientation==Y_AXIS) {
			if (rnd(2)==0) {
				cube.x += rndPM();
			} else {
				cube.z += rndPM();
			}
		}
		else if (orientation==Z_AXIS) {
			if (rnd(2)==0) {
				cube.x += rndPM();
			} else {
				cube.y += rndPM();
			}
		}
		
		return cube;
	}
	
	/**
	 * Returns either -1 or 1;
	 * @return
	 */
	private static int rndPM() {
		return Math.random()<0.5?-1:1;
	}
	
	private static int rnd(int n) {
		return (int)(n*Math.random());
	}
	
	private static int getOrientation(Cube cube) {
		if (cube.height==1&&cube.depth==1) {
			return X_AXIS;
		} else if (cube.width==1&&cube.depth==1) {
			return Y_AXIS;
		} else if (cube.width==1&&cube.height==1) {
			return Z_AXIS;
		} else {
			// 1x1x1:
			return rnd(3);
		}
	}
	
	
	
	
	/**
	 * Reference to parent PApplet object;
	 */
	private CubesEclipse2 parent;
	/**
	 * 'top', 'left', 'back' corner of cube
	 */
	private int x, y, z;
	
	/**
	 * Dimensions of cube in SPACE units
	 */
	private int width, height, depth;
	
	/**
	 * Number of children
	 */
	private int nrOfChildren = 0;
	
	/**
	 * Number of birth tries
	 */
	private int birthTries = 0;
	
	/*private Cube(CubesEclipse2 parent, int x, int y, int z, int width, int height, int depth) {
		this(x,y,z,width,height,depth);
		this.parent = parent;
	}*/
	
	private Cube(int x, int y, int z, int width, int height, int depth) {
		this.x=x;
		this.y=y;
		this.z=z;
		this.width=width;
		this.height=height;
		this.depth=depth;
	}

	public void draw() {
		parent.drawBoxAt(x, y, z, width, height, depth);
	}

	public int getBirthTries() {
		return birthTries;
	}

	public int getNrOfChildren() {
		return nrOfChildren;
	}

	public void increaseBirthTries() {
		birthTries++;
	}

	public void increaseNrOfChildren() {
		nrOfChildren++;
	}
	
	/**
	 * Sets the occupied array to <code>false</code> at the position where the <code>Cube</code> is.
	 * <p>
	 * If the <code>Cube</code> is anywhere out of bounds, does not set the array and returns {@value Cube#OUT_OF_BOUNDS}.
	 * <p>
	 * If the <code>Cube</code> is trying to occupy an occupied space, does not set the array and return {@link Cube#OCCUPIED}
	 * @param occupied the occupied array to set
	 * @return
	 */
	private int testSpace(boolean[] occupied, int[] occupiedIndex, int[] occupiedColor, int newIndex, int newColor) {
		
		ArrayList<Integer> indexesToSet = new ArrayList<Integer>();
		for (int zz=0;zz<depth;zz++) {
			for (int yy=0;yy<height;yy++) {
				for (int xx=0;xx<width;xx++) {
					int zzz = z+zz;
					int yyy = y+yy;
					int xxx = x+xx;
					if (	xxx<0||xxx>=parent.SPACE_WIDTH
							||yyy<0||yyy>=parent.SPACE_HEIGHT
							||zzz<0||zzz>=parent.SPACE_DEPTH) {
						return OUT_OF_BOUNDS;
					} else {
						int index = zzz*parent.zDim + yyy*parent.yDim + xxx;
						if (occupied[index]) {
							return OCCUPIED;
						}
						indexesToSet.add(new Integer(index));
					}
				}
			}
		}
		
		for (int i=0;i<indexesToSet.size();i++) {
			int ii = indexesToSet.get(i);
			occupied[ii] = true;
			occupiedIndex[ii] = newIndex;
			occupiedColor[ii] = newColor;
		}
		
		return OK;
	}

	public String toString() {
		return "Cube ["+width+","+height+","+depth+"] at ["+x+","+y+","+z+"]";
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	
	
}
