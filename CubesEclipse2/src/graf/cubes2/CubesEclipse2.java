package graf.cubes2;

import processing.core.PApplet;
import processing.core.PVector;

public class CubesEclipse2 extends PApplet {

	private static final long serialVersionUID = -8820715366194276155L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.cubes2.CubesEclipse2"});
	}
	
	/**
	 * Maximum amount of cubes to be created
	 */
	private final int CUBE_MAXIMUM = 300;
	
	/**
	 * Max length of a <code>Cube</code> to be created
	 */
	private final int MAX_CUBE_LENGTH = 10;
	
	/**
	 * Max children added per frame
	 */
	private final int MAX_CHILDREN_PER_FRAME = 10;
	
	/**
	 * Max children per Cube
	 */
	private final int MAX_CHILDREN_PER_CUBE = 3;
	
	/**
	 * Max tries of reproduction
	 */
	private final int MAX_REPRODUCTION_TRIES = 10;
	
	/**
	 * The space is divided in cubes. This is its width
	 */
	final int SPACE_WIDTH = 40;
	
	/**
	 * The space is divided in cubes. This is its height
	 */
	final int SPACE_HEIGHT = 20;
	
	/**
	 * The space is divided in cubes. This is its d
	 */
	final int SPACE_DEPTH = 20;
	
	/**
	 * The factor to scale the <code>occupied</code> to the screen coordinates
	 */
	private final float SPACE_TO_SCREEN_FAC = 20; 
	
	/**
	 * Screen width (SPACE_WIDTH * SPACE_TO_SCREEN_FAC)
	 */
	private final float screenWidth = SPACE_WIDTH*SPACE_TO_SCREEN_FAC;
	/**
	 * Screen height (SPACE_HEIGHT*SPACE_TO_SCREEN_FAC)
	 */
	private final float screenHeight = SPACE_HEIGHT*SPACE_TO_SCREEN_FAC;
	/**
	 * Screen depth (SPACE_DEPTH*SPACE_TO_SCREEN_FAC)
	 */
	private final float screenDepth = SPACE_DEPTH*SPACE_TO_SCREEN_FAC;
	
	/**
	 * Boolean array that keeps track if a cube in space is occupied.
	 * This array is a 1-dimensional array actually representing 3 dimensions.
	 * Its dimensions are {@link #SPACE_WIDTH} by {@link #SPACE_HEIGHT} by {@link #SPACE_DEPTH}
	 */
	private final boolean[] occupied = new boolean[SPACE_WIDTH*SPACE_HEIGHT*SPACE_DEPTH];
	
	private final int[] occupiedIndex = new int[occupied.length];
	
	private final int[] occupiedColor = new int[occupied.length];
	
	/**
	 * used for converting y-coordinate to the 1-dimensional array coordinate of {@link #occupied}
	 */
	final int yDim = SPACE_WIDTH;
	/**
	 * used for converting z-coordinate to the 1-dimensional array coordinate of {@link #occupied}
	 */
	final int zDim = SPACE_WIDTH*SPACE_HEIGHT;
	
	/**
	 * Double-click time in milliseconds
	 */
	private final int DOUBLE_CLICK_TIME = 200;
	
	/**
	 * Center of scene in screen coordinates
	 */
	private final PVector centerOfScene = new PVector(
				screenWidth*0.5f,
				screenHeight*0.5f,
				screenDepth*0.5f
				);
	
	private final PVector cameraOffset = new PVector(
			0,
			0,
			SPACE_TO_SCREEN_FAC*Math.max(Math.max(SPACE_WIDTH, SPACE_HEIGHT),SPACE_DEPTH));
	
	/**
	 * Number of cubes created
	 */
	private int liveCubes = 0;
	
	/**
	 * Cube array
	 */
	private final Cube[] cube = new Cube[CUBE_MAXIMUM];
	
	/**
	 * last millis() when mouse was pressed. Used for detecting double click.
	 */
	private int lastMousePressed = 0;

	private static final int OCCUPIED = 0;
	private static final int TOUCHING = 1;
	
	private int displayMode = OCCUPIED;
	
	
	public void settings() {
		size(800,600,P3D);
	}
	/**
	 * Setup Main
	 */
	public void setup() {
		
		//smooth();
		initialize();	
	}
	
	private void initialize() {
		for (int i=0;i<occupied.length;i++) {
			occupied[i]=false;
			occupiedIndex[i] = -1;
			occupiedColor[i]=0xffff4020;
		}
		
		liveCubes=0;
		while (liveCubes<5) {
			Cube c = Cube.randomCube(
					SPACE_WIDTH, 
					SPACE_HEIGHT, 
					SPACE_DEPTH,
					MAX_CUBE_LENGTH);
			cube[liveCubes] = Cube.placeCube(this, c, occupied, occupiedIndex, occupiedColor, liveCubes, 0xff40ff20);
			if (cube[liveCubes]!=null) {
				liveCubes++;
			}
		}
	}
	
	private void doubleClick() {
		initialize();
		println("X");
	}
	
	public void mousePressed() {
		if (mouseButton==LEFT) {
			int ms = millis();
			int timePassed = ms-lastMousePressed;
			lastMousePressed = ms;
			if (timePassed<DOUBLE_CLICK_TIME) {
				doubleClick();
			}
		}
	}
	
	
	public void mouseDragged() {
		if (mouseButton==LEFT) {
			cameraOffset.add(pmouseX-mouseX,pmouseY-mouseY,0);
		} else if (mouseButton==RIGHT) {
			cameraOffset.add(0,0,pmouseY-mouseY);
		}
		
	}
	
	private void changeDisplayMode() {
		if (displayMode==OCCUPIED) {
			displayMode=TOUCHING;
		} else if (displayMode==TOUCHING) {
			displayMode=OCCUPIED;
		}
	}
	
	public void keyPressed() {
		changeDisplayMode();
	}
	
	
	private void createChildren() {
		
		
		/*
		 * Check if any more children wanted
		 */
		if (liveCubes<cube.length) {
			print("CHILDREN: ");
			
			int addedThisFrame = 0;
			
			/*
			 * Go through all existing cubes
			 */
			for (int i=0;i<liveCubes;i++) {
				
				/*
				 * Check if the cube needs more children
				 * and did not have enough tries yet
				 */
				boolean canHaveMoreChildren = (cube[i].getNrOfChildren()<MAX_CHILDREN_PER_CUBE);
				boolean didNotHaveEnoughTries = (cube[i].getBirthTries()<MAX_REPRODUCTION_TRIES);
				boolean totalAmountNotSurpasseed = (addedThisFrame + liveCubes)<cube.length;
				
				if (canHaveMoreChildren && didNotHaveEnoughTries && totalAmountNotSurpasseed) {
					
					/*
					 * Make a random cube touching this cube
					 */
					Cube c = Cube.randomCubeTouching(SPACE_WIDTH, SPACE_HEIGHT, SPACE_DEPTH, MAX_CUBE_LENGTH, cube[i]);
					cube[i].increaseBirthTries();
					
					int childIndex = liveCubes+addedThisFrame;
					if (childIndex<cube.length && addedThisFrame<MAX_CHILDREN_PER_FRAME) {
						int sourceIndex = cube[i].getZ()*zDim + cube[i].getY()*yDim + cube[i].getX();
						int sourceColor = occupiedColor[sourceIndex];
						sourceColor = morphColor(sourceColor);
						cube[childIndex] = Cube.placeCube(this, c, occupied, occupiedIndex, occupiedColor, childIndex, sourceColor);
						if (cube[childIndex]!=null) {
							cube[i].increaseNrOfChildren();
							addedThisFrame++;
						}
					}

				}
				
			}
			
			liveCubes += addedThisFrame;
			println(addedThisFrame+" ("+liveCubes+")");
			
		}
		
		
		
		
	}
	
	
	private int morphColor(int sourceColor) {
		
		int totalChange = 100;
		
		int r = (int)red(sourceColor);
		int g = (int)green(sourceColor);
		int b = (int)blue(sourceColor);
		
		int dirR = Math.random()<0.5?-1:1;
		int dirG = Math.random()<0.5?-1:1;
		int dirB = Math.random()<0.5?-1:1;
		
		for (int i=0;i<totalChange;i++) {
			int channel = (int)(3*Math.random());
			if (channel==0) {
				r+=dirR;
			} else if (channel==1) {
				g+=dirG;
			} else {
				b+=dirB;
			}
		}
		
		return color(r,g,b);
		
	}

	private int lastms=0;
	
	public void draw() {
		
		createChildren();
		
		if (millis()-lastms>1000) {
			changeDisplayMode();
			lastms=millis();
		}
		
		background(250);		
		
		/*
		 * Look at center of scene
		 */
		camera(centerOfScene.x+cameraOffset.x, centerOfScene.y+cameraOffset.y, centerOfScene.z+cameraOffset.z,
				centerOfScene.x, centerOfScene.y, centerOfScene.z,
				0,1,0);
		
		/*
		 * Light
		 */
		pointLight(200, 200, 200, 
				-1*SPACE_WIDTH*SPACE_TO_SCREEN_FAC, 
				-1*SPACE_HEIGHT*SPACE_TO_SCREEN_FAC, 
				3*SPACE_DEPTH*SPACE_TO_SCREEN_FAC);
		
		/*
		 * Draw SPACE
		 */
		stroke(128,128,128,64);
		noFill();
		drawBoxAt(0, 0, 0, SPACE_WIDTH, SPACE_HEIGHT, SPACE_DEPTH);
		
		//stroke(128,128,128,64);
		//drawSpaceEdges();
		
		if (displayMode==OCCUPIED) {
			/*
			 * 	Draw occupied
		 	*/
			noStroke();
			drawOccupied(64);
		}
		
		else if (displayMode==TOUCHING) {
			noStroke();
			fill(255,32,32,32);
			drawTouching();
		}
		
		//fill(255,32,32,32);
		//stroke(0,0,0);
		//drawCubes();
//		saveFrame("output/CubesEclipse2_###.jpg");
	}

	private void drawTouching() {
		
		int index=0;
		for (int z=0;z<SPACE_DEPTH;z++) {
			for (int y=0;y<SPACE_HEIGHT;y++) {
				for (int x=0;x<SPACE_WIDTH;x++) {
					boolean hereOccupied = occupied[index];
					if (hereOccupied) {
						// border right:
						if (x<SPACE_WIDTH-1) {
							if (occupied[index+1] && occupiedIndex[index+1]!=occupiedIndex[index]) {
								PVector c0 = PVector.mult(new PVector(x+1,y,z), SPACE_TO_SCREEN_FAC);
								PVector c1 = PVector.mult(new PVector(x+1,y+1,z), SPACE_TO_SCREEN_FAC);
								PVector c2 = PVector.mult(new PVector(x+1,y+1,z+1), SPACE_TO_SCREEN_FAC);
								PVector c3 = PVector.mult(new PVector(x+1,y,z+1), SPACE_TO_SCREEN_FAC);
								beginShape(QUAD);
								vertex(c0,c1,c2,c3);
								endShape();
							}
						}
						// border bottom
						if (y<SPACE_HEIGHT-1) {
							if (occupied[index+yDim] && occupiedIndex[index+yDim]!=occupiedIndex[index]) {
								PVector c0 = PVector.mult(new PVector(x,y+1,z), SPACE_TO_SCREEN_FAC);
								PVector c1 = PVector.mult(new PVector(x+1,y+1,z), SPACE_TO_SCREEN_FAC);
								PVector c2 = PVector.mult(new PVector(x+1,y+1,z+1), SPACE_TO_SCREEN_FAC);
								PVector c3 = PVector.mult(new PVector(x,y+1,z+1), SPACE_TO_SCREEN_FAC);
								beginShape(QUAD);
								vertex(c0,c1,c2,c3);
								endShape();
							}
						}
						// border front
						if (z<SPACE_DEPTH-1) {
							if (occupied[index+zDim] && occupiedIndex[index+zDim]!=occupiedIndex[index]) {
								PVector c0 = PVector.mult(new PVector(x,y,z+1), SPACE_TO_SCREEN_FAC);
								PVector c1 = PVector.mult(new PVector(x+1,y,z+1), SPACE_TO_SCREEN_FAC);
								PVector c2 = PVector.mult(new PVector(x+1,y+1,z+1), SPACE_TO_SCREEN_FAC);
								PVector c3 = PVector.mult(new PVector(x,y+1,z+1), SPACE_TO_SCREEN_FAC);
								beginShape(QUAD);
								vertex(c0,c1,c2,c3);
								endShape();
							}
						}
					}
					index++;
				}
			}
		}
		
	}

	private void vertex(PVector... vecs) {
		for (PVector vec:vecs) {
			vertex(vec.x,vec.y,vec.z);
		}
	}

	private void line(PVector v0, PVector v1) {
		line(v0.x,v0.y,v0.z,v1.x,v1.y,v1.z);
	}

	/**
	 * Draws all cubes
	 */
	private void drawCubes() {
		for (int i=0;i<liveCubes;i++) {
			cube[i].draw();
		}
	}

	/**
	 * Draws all occupied space
	 *
	 */
	private void drawOccupied(int alpha) {
		int index = 0;
		for (int z=0;z<SPACE_DEPTH;z++) {
			for (int y=0;y<SPACE_HEIGHT;y++) {
				for (int x=0;x<SPACE_WIDTH;x++) {
					if (occupied[index]) {
						if (alpha==255) {
							fill(occupiedColor[index]);
						} else {
							int c = occupiedColor[index];
							fill(red(c),green(c),blue(c),alpha);
						}
						drawBoxAt(x, y, z, 1, 1, 1);
					}
					index++;
				}
			}
		}
	}


	/**
	 * Draws the space edges
	 */
	private void drawSpaceEdges() {
		for (int z=0;z<SPACE_DEPTH+1;z++) {
			float zz = z*SPACE_TO_SCREEN_FAC;
			line(0,0,zz, screenWidth,0,zz);
			line(screenWidth,0,zz, screenWidth,screenHeight,zz);
			line(screenWidth,screenHeight,zz, 0,screenHeight,zz);
			line(0,screenHeight,zz, 0,0,zz);
		}
		
		for (int x=0;x<SPACE_WIDTH+1;x++) {
			float xx = x*SPACE_TO_SCREEN_FAC;
			line(xx,0,0, xx,screenHeight,0);
			line(xx,screenHeight,0, xx,screenHeight,screenDepth);
			line(xx,screenHeight,screenDepth, xx,0,screenDepth);
			line(xx,0,screenDepth, xx,0,0);
		}
		
		for (int y=0;y<SPACE_HEIGHT+1;y++) {
			float yy = y*SPACE_TO_SCREEN_FAC;
			line(0,yy,0, screenWidth,yy,0);
			line(screenWidth,yy,0, screenWidth,yy,screenDepth);
			line(screenWidth,yy,screenDepth, 0,yy,screenDepth);
			line(0,yy,screenDepth, 0,yy,0);
		}
	}
	
	public void drawBoxAt(int x, int y, int z, int width, int height, int depth) {
		PVector trans = PVector.mult(new PVector(x,y,z), SPACE_TO_SCREEN_FAC);
		PVector box = PVector.mult(new PVector(width,height,depth), SPACE_TO_SCREEN_FAC);
		trans.add( PVector.mult(box, 0.5f) );
		pushMatrix();
		translate(trans.x,trans.y,trans.z);
		box(box.x,box.y,box.z);
		popMatrix();
	}
	
	/*public void drawRect(int direction, int x, int y, int z) {
		PVector trans = PVector.mult(new PVector(x,y,z), SPACE_TO_SCREEN_FAC);
		
	}*/
	
		
	/*private boolean isOccupied(int x, int y, int z) {
		if (x<0||x>=SPACE_WIDTH||y<0||y>=SPACE_HEIGHT||z<0||z>=SPACE_DEPTH) {
			throw new RuntimeException("Index out of bounds: "+x+","+y+","+z);
		} else {
			return occupied[zDim*z + yDim*y + x];
		}
	}*/	

}
